CREATE TABLE customers (
                           id INT PRIMARY KEY NOT NULL,
                           email_address VARCHAR(255) NOT NULL,
                           name VARCHAR(255),
                           UNIQUE (email_address)
);