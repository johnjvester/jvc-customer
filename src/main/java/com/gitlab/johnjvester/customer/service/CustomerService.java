package com.gitlab.johnjvester.customer.service;

import com.gitlab.johnjvester.customer.entities.Customer;
import com.gitlab.johnjvester.customer.repositories.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@RequiredArgsConstructor
@Service
public class CustomerService {
    private final CustomerRepository customerRepository;

    public List<Customer> getCustomers() {
        return customerRepository.findAll();
    }

    public Customer getCustomer(int id) {
        return customerRepository.findById(id).orElseThrow(NoSuchElementException::new);
    }
}
