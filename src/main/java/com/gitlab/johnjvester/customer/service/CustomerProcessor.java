package com.gitlab.johnjvester.customer.service;

import com.gitlab.johnjvester.customer.entities.Customer;
import com.gitlab.johnjvester.customer.models.CustomerDto;
import com.gitlab.johnjvester.customer.repositories.CustomerRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@RequiredArgsConstructor
@Component
public class CustomerProcessor {
    private final CustomerRepository customerRepository;

    @RabbitListener(queues = "#{messagingConfigurationProperties.customerRequestQueue}")
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public CustomerDto receive(CustomerDto customerDto) {
        log.debug("CustomerProcessor: receive(customerDto={})", customerDto);

        Customer customer = customerRepository.findByEmailAddressEquals(customerDto.getEmailAddress());

        if (customer != null) {
            log.debug("Found existing customer={}", customer);
            customerDto = new CustomerDto();
            customerDto.setId(customer.getId());
            customerDto.setEmailAddress(customer.getEmailAddress());
            customerDto.setName(customer.getName());
        } else {
            log.info("Creating new customer={}", customerDto);
            customer = new Customer();
            customer.setEmailAddress(customerDto.getEmailAddress());
            customer.setName(customerDto.getName());
            customerRepository.save(customer);
            customerDto.setId(customer.getId());
        }

        log.debug("customerDto={}", customerDto);
        return customerDto;
    }
}
