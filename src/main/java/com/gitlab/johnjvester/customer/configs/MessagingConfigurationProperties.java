package com.gitlab.johnjvester.customer.configs;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.validation.constraints.NotNull;

@Data
@Configuration("messagingConfigurationProperties")
@ConfigurationProperties(prefix = "messaging")
public class MessagingConfigurationProperties {
    @NotNull
    private String customerDirectExchange;

    @NotNull
    private String customerRequestQueue;

    @NotNull
    private String customerRoutingKey;
}
