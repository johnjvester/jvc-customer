package com.gitlab.johnjvester.customer.configs;

import lombok.RequiredArgsConstructor;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@RequiredArgsConstructor
@Configuration
@EnableRabbit
public class MessagingConfiguration {
    private final MessagingConfigurationProperties messagingConfigurationProperties;

    @Bean(name = "customerDirectExchange")
    public DirectExchange customerDirectExchange() {
        return new DirectExchange(messagingConfigurationProperties.getCustomerDirectExchange());
    }

    @Bean(name = "customerRequestQueue")
    public Queue customerRequestQueue() {
        return new Queue(messagingConfigurationProperties.getCustomerRequestQueue());
    }

    @Bean
    public Binding binding(DirectExchange customerDirectExchange,
                           Queue queue) {
        return BindingBuilder.bind(queue)
                .to(customerDirectExchange)
                .with(messagingConfigurationProperties.getCustomerRoutingKey());
    }

    @Bean
    public MessageConverter jackson2MessageConverter() {
        return new Jackson2JsonMessageConverter();
    }
}
