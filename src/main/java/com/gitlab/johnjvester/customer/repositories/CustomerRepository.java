package com.gitlab.johnjvester.customer.repositories;

import com.gitlab.johnjvester.customer.entities.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Integer> {
    Customer findByEmailAddressEquals(String emailAddress);
}
