package com.gitlab.johnjvester.customer.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/*
This object would really be in a shared Java library.
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class CustomerDto {
    private int id;
    private String emailAddress;
    private String Name;
}
